import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { Task } from "./task";

@Injectable({
  providedIn: "root"
})

export class TaskService{

  constructor(private http: HttpClient){}

  getTasks(): Observable<Task[]> {
    return this.http.get("http://192.168.15.3:3000/tasks").pipe(map(
        res => {
            let results = res as Task[];
            return results;
        }
    ));
  }

  getTask(id: string): Promise<Task> {
    return fetch('http://192.168.15.3:3000/tasks/details/' + id).then(res => res.json());
  }

}
