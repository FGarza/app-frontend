import { Component, OnInit } from "@angular/core";
import { Observable } from "rxjs";

import { Task } from "./task";
import { TaskService } from "./task.service";

@Component({
  selector: "ns-tasks",
  moduleId: module.id,
  templateUrl: "./tasks.component.html"
})

export class TasksComponent implements OnInit{

  public tasks: Observable<Task[]>;

  constructor(private taskServie: TaskService) { }

  ngOnInit(): void {
    this.tasks = this.taskServie.getTasks();
  }
}
